import java.io.StringReader;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Wookie on 2016-05-02.
 */
public class SimpleApplication {
    private BagOfWordsBuilder builder;
    private Map<String, Map<Integer,Double>> filesVectors;
    private Map<Integer, Integer> wordInfluence;
    public final Function<Integer,Double> defaultWordImportance = integer -> (double)filesVectors.size() / wordInfluence.get(integer);
    public final Function<Integer,Double> defaultIdentity = integer -> 1.0;

    public SimpleApplication(String dataRoot) {
        builder = new BagOfWordsBuilder(dataRoot);
        this.wordInfluence = builder.getSolution().keySet()
                .parallelStream()
                .collect(Collectors.toMap(s -> builder.getSolution().get(s), s -> 0));
        for (Map<Integer,Integer> map: builder.getAllFilesVectors().values()) {
            Set<Integer> tmp = map.keySet().stream().filter(integer -> map.get(integer) > 0).collect(Collectors.toSet());
            wordInfluence.forEach(((int1, int2) -> {
                if (tmp.contains(int1)) {
                    wordInfluence.put(int1, int2 + 1);
                }
            }));
        }
        this.filesVectors = builder.getAllFilesVectors()
                .keySet()
                .parallelStream()
                .collect(Collectors.toMap(s -> s, s -> {
                    Map<Integer,Integer> tmp = builder.getAllFilesVectors().get(s);
                    return tmp
                            .keySet()
                            .parallelStream()
                            .collect(Collectors.toMap(integer -> integer,integer -> Double.valueOf(tmp.get(integer))));
                }));
    }

    public int getWordsNumber() {
        return wordInfluence.size();
    }


    public Map<Integer, Double> getFileVectorWithImportance(String fileName) {
        return getFileVectorWithImportance(fileName, defaultWordImportance);
    }

    public Map<Integer, Double> getFileVectorWithImportance(String fileName, Function<Integer,Double> importance) {
        Map<Integer,Double> tmpMap = filesVectors.get(fileName);
        return tmpMap
                .keySet()
                .parallelStream()
                .collect(Collectors.toMap(integer -> integer,integer -> tmpMap.get(integer) * importance.apply(integer)));
    }

    public static Map<Integer, Double> normalised(Map<Integer, Double> vector) {
        Double norm = vector.keySet()
                .parallelStream()
                .map(vector::get)
                .reduce(0.0,(aDouble, aDouble2) -> aDouble + aDouble2*aDouble2, (aDouble, aDouble2) -> aDouble + aDouble2);
        return vector.keySet()
                .parallelStream()
                .collect(Collectors.toMap(integer -> integer,integer -> vector.get(integer) / norm));
    }

    public Set<String> getAllFiles() {
        return filesVectors.keySet();
    }

    public Map<Integer,Double> getStringVectorWithImportance(String data) {
        return getStringVectorWithImportance(data, defaultWordImportance);
    }

    public Map<Integer,Double> getStringVectorWithImportance(String data, Function<Integer,Double> importance) {
        StringReader reader = new StringReader(data);
        Map<String, Integer> tmp = BagOfWordsBuilder.getReaderSolution(reader);
        return tmp
                .keySet()
                .parallelStream()
                .filter(s -> builder.getSolution().containsKey(s))
                .collect(Collectors
                        .toMap(
                                s -> builder.getSolution().get(s),
                                s -> tmp.get(s) * importance.apply(builder.getSolution().get(s))));
    }
}
