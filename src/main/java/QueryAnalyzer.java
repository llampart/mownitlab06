import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Wookie on 2016-05-01.
 */
public class QueryAnalyzer {
    private final SimpleApplication application;
    private Map<String, Map<Integer,Double>> normalisedVectors = new HashMap<>();

    private class VectorsComparator implements Comparator<String> {
        private final Map<Integer, Double> query;

        public VectorsComparator(Map<Integer, Double> query) {
            this.query = query;
        }

        @Override
        public int compare(String o1, String o2) {
            if (getAngleToQuery(o1) < getAngleToQuery(o2)) {
                return 1;
            }
            else {
                return -1;
            }
        }

        private double getAngleToQuery(String o) {
            Map<Integer, Double> tmp = normalisedVectors.get(o);
            return query.keySet()
                    .parallelStream()
                    .map(integer -> query.get(integer) * tmp.getOrDefault(integer, 0.0))
                    .reduce(0.0, (aDouble, aDouble2) -> aDouble + aDouble2, (aDouble, aDouble2) -> aDouble + aDouble2);
        }
    }

    public QueryAnalyzer(SimpleApplication application, int approximationLevel) {
        this.application = application;
        initialiseVectors();
        MatrixFilter filter = new MatrixFilter(normalisedVectors,application.getWordsNumber());
        filter.removeNoise(approximationLevel);
    }

    private void initialiseVectors() {
        for (String s: application.getAllFiles()) {
            normalisedVectors.put(s,SimpleApplication.normalised(application.getFileVectorWithImportance(s)));
        }
    }

    public List<String> listTheBestFiles(int limit,String query) {
        Map<Integer,Double> queryVector = SimpleApplication.normalised(application.getStringVectorWithImportance(query));
        return normalisedVectors
                .keySet()
                .parallelStream()
                .sorted(new VectorsComparator(queryVector))
                .limit(limit)
                .collect(Collectors.toList());
    }

    /**
     * Simple query handling application.
     * @param args - starting paramters of application: args[0] - path to the documents directory,
     *             args[1] - number of documents name displayed in one query
     *             args[2] - k parameter in low rank approximation
     *             There have to be at least 3 arguments for the program to execute properly
     * @throws IOException if there is some problem with reading from stdin
     */
    public static void main(String[] args) throws IOException {
        BagOfWordsBuilder.readStopWords("stopwords.txt");
        QueryAnalyzer mainAnalyzer = new QueryAnalyzer(new SimpleApplication(args[0]),Integer.valueOf(args[2]));
        boolean working = true;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (working) {
            System.out.println("Type your query:");
            System.out.println(mainAnalyzer.listTheBestFiles(Integer.valueOf(args[1]),reader.readLine()));
            System.out.println("Do you wish to exit the program? (type yes if you do, default action is to continue)");
            String tmp = reader.readLine();
            if (tmp.contains("yes")) {
                working = false;
            }
        }
    }

}
