/**
 * Created by Wookie on 2016-04-16.
 */
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseTokenizer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.en.KStemFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.AttributeFactory;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class BagOfWordsBuilder {
    private final String rootDirectory;
    private Map<String,Integer> solution = null;
    private Map<String, Map<Integer,Integer>> wordCountSolution = null;
    private static CharArraySet stopWords = EnglishAnalyzer.getDefaultStopSet();
    public BagOfWordsBuilder(String rootDirectory) {
        this.rootDirectory = rootDirectory;
    }

    private class WordsChecker implements Consumer<File> {
        @Override
        public void accept(File file) {
            Reader a = null;
            try {
                a = new InputStreamReader(new FileInputStream(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Tokenizer b =new LowerCaseTokenizer(AttributeFactory.DEFAULT_ATTRIBUTE_FACTORY);
            b.setReader(a);
            TokenFilter c =new StopFilter(b, stopWords);
            TokenFilter filter = new KStemFilter(c);
            CharTermAttribute value = filter.addAttribute(CharTermAttribute.class);
            try {
                filter.reset();
                while (filter.incrementToken()) {
                    if (!solution.containsKey(value.toString())) {
                        solution.put(value.toString(),solution.size());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class WordsCounter implements Consumer<File> {

        @Override
        public void accept(File file) {
            Reader a = null;
            Map<Integer, Integer> fileMapHandle = new HashMap<>();
            try {
                a = new InputStreamReader(new FileInputStream(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Tokenizer b =new LowerCaseTokenizer(AttributeFactory.DEFAULT_ATTRIBUTE_FACTORY);
            b.setReader(a);
            TokenFilter c =new StopFilter(b, stopWords);
            TokenFilter filter = new KStemFilter(c);
            CharTermAttribute value = filter.addAttribute(CharTermAttribute.class);
            try {
                filter.reset();
                while (filter.incrementToken()) {
                    Integer tmp = fileMapHandle.getOrDefault(solution.get(value.toString()),0);
                    fileMapHandle.put(solution.get(value.toString()),tmp + 1);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            wordCountSolution.put(file.getName(),fileMapHandle);
        }
    }


    public Map<String, Integer> getSolution() {
        if (solution != null) return solution;
        else {
            solution = new TreeMap<>();
            solve(new File(rootDirectory), new WordsChecker());
        }
        return solution;
    }

    public Map<String,Map<Integer, Integer>> getAllFilesVectors() {
        if (solution == null) getSolution();
        if (wordCountSolution != null) return wordCountSolution;
        else {
            wordCountSolution = new HashMap<>();
            solve(new File(rootDirectory), new WordsCounter());
            return wordCountSolution;
        }
    }

    public static Map<String,Integer> getReaderSolution(Reader reader) {
        Map<String, Integer> tmpMap = new HashMap<>();
        Tokenizer lowerToken = new LowerCaseTokenizer(AttributeFactory.DEFAULT_ATTRIBUTE_FACTORY);
        lowerToken.setReader(reader);
        TokenFilter stopFilter = new StopFilter(lowerToken, stopWords);
        TokenFilter stemFilter = new KStemFilter(stopFilter);
        CharTermAttribute value = stemFilter.addAttribute(CharTermAttribute.class);
        try {
            stemFilter.reset();
            while (stemFilter.incrementToken()) {
                Integer tmp = tmpMap.getOrDefault(value.toString(),0);
                tmpMap.put(value.toString(),tmp + 1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tmpMap;
    }

    private void solve(File currentRoot, Consumer<File> consumer) {
        File [] list = currentRoot.listFiles();
        list = (list == null ? new File[0] : list);
        for (File fil : list) {
            if (fil.isDirectory()) {
                solve(fil,consumer);
            }
            else {
                if (fil.getName().substring(fil.getName().lastIndexOf('.') + 1).equals("txt")) {
                    consumer.accept(fil);
                }
            }
        }
    }

    public static void readStopWords(String stopFile) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(stopFile));
            stopWords = new CharArraySet(reader
                    .lines()
                    .flatMap(s -> Arrays.stream(s.split(" "))).collect(Collectors.toList()), true);
            System.out.println(stopWords);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
