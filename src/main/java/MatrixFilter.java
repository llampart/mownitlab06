import org.apache.commons.math3.linear.OpenMapRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.SingularValueDecomposition;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Wookie on 2016-05-02.
 */


public class MatrixFilter {
    RealMatrix data;
    Map<String,Integer> filesNumbersMap;
    Map<String, Map<Integer,Double>> fileVector;
    int rowNumber,columnNumber;
    MatrixFilter(Map<String, Map<Integer,Double>> fileVector, int wordsNumber) {
        rowNumber = fileVector.size();
        filesNumbersMap = new HashMap<>();
        columnNumber = wordsNumber;
        data = new OpenMapRealMatrix(rowNumber,columnNumber);
        this.fileVector = fileVector;
        int maxind = 0;
        for (String s: fileVector.keySet()) {
            filesNumbersMap.put(s,maxind);
            ++maxind;
        }
        fileVector.forEach((s, integerStringMap) -> {
            integerStringMap.forEach((integer, double1) -> {
                data.setEntry(filesNumbersMap.get(s),integer,double1);
            });
        });
    }

    void removeNoise(int k) {
        k = Integer.min(k,Integer.min(rowNumber,columnNumber) - 1);
        System.out.println(k);
        SingularValueDecomposition decomposition = new SingularValueDecomposition(data);
        RealMatrix uDecomposed = decomposition.getU().getSubMatrix(0,rowNumber - 1,0,k);
        RealMatrix dDecomposed = decomposition.getS().getSubMatrix(0,k,0,k);
        RealMatrix vtDecomposed = decomposition.getVT().getSubMatrix(0,k,0,columnNumber - 1);
        RealMatrix approximated = uDecomposed.multiply(dDecomposed).multiply(vtDecomposed);
        fileVector.forEach(((s, integerDoubleMap) -> {
            fileVector.put(s,integerDoubleMap.
                    keySet().
                    parallelStream().
                    collect(Collectors.toMap(integer -> integer,integer ->approximated.getEntry(filesNumbersMap.get(s),integer))));
        }));
    }
}
