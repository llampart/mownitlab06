import org.apache.lucene.index.ExitableDirectoryReader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Wookie on 2016-05-02.
 */
public class SimpleApplicationTest {
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    private final String root = "D:\\chwilowka\\llampart\\parser";//needs absolute path to files directory

    @Test
    public void printFileVector() throws Exception {
        SimpleApplication application = new SimpleApplication(root);
        System.out.println(application.getFileVectorWithImportance("computers.txt"));
        System.out.println(application.getFileVectorWithImportance("computers.txt",application.defaultIdentity));
        System.out.println(SimpleApplication.normalised(application.getFileVectorWithImportance("computers.txt")));
    }

    @Test
    public void getStringVector() throws Exception {
        SimpleApplication application = new SimpleApplication(root);
        System.out.println(application.getStringVectorWithImportance("Pizza and Computers, Computers and my love!"));
    }
}